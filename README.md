# Pepper&Carrot Book-Publishing

This repository was created to host and manage the multilingual comic book
publishing of [Pepper&Carrot](https://www.peppercarrot.com).

# Install/Setup

The book-publishing project was made and rendered on Kubuntu 20.04LTS in July/August 2020. I precise it here because it mean in theory all 'buntu 20.04 based Operating System will not have trouble to get the good compatible version to open and work on the files. In theory, other more recent distributions should also have no problem, but I cannot warranty that.

## Libraries & Software

LaTex/TexLive full
Imagemagick
Scribus 1.5.5 (!strict −if possible− no SVN/Git version: Scribus files are not backward compatible at all)
Inkscape 1.0 (or greater)
Krita 4.3 Appimage (or greater, but use appimage)
Fontforge (Optional, to edit font and add missing characters)

A one liner on 20.04:

    sudo apt install scribus fontforge fontforge-extras texlive-full texlive-extra-utils pdftk pandoc git wget imagemagick diffutils rsync curl cryptsetup libgdk-pixbuf2.0-dev libxml2-utils libcurl4 unrar zip unzip

### Fix Imagemagick memory limit.

By default, Imagemagick, a command line tool to manipulate raster images, is installed with a very low tolerance to large files. If we want to convert 300ppi PNG or JPG to CMYK this faulty default will return errors. To level up the possibilities of this fantastic library, edit this file as administrator (sudo):

    /etc/ImageMagick-6/policy.xml 

Scroll the lines and upgrade the limit manually with the text editor. My setup use that:

    <policy domain="resource" name="memory" value="4GiB"/>
    <policy domain="resource" name="map" value="4GiB"/>
    <policy domain="resource" name="width" value="128KP"/>
    <policy domain="resource" name="height" value="128KP"/>
    <policy domain="resource" name="area" value="4GiB"/>
    <policy domain="resource" name="disk" value="6GiB"/>

also, allow PDF read/write (it is useful).  
Find the line:

    <policy domain="coder" rights="none" pattern="PDF" />

and change it to

    <policy domain="coder" rights="read | write" pattern="PDF" />

## Color Profile:

The bookpublishing −If printed with drivethrucomics POD− will require two profiles, the CMYK one of the printer named CGATS21_CRPC1.icc and the one I use on Pepper&Carrot RGB source files: sRGB-elle-V2-srgbtrc.icc . You need to install this two profiles in this folder so Scribus and Krita can catch them:

    mkdir $HOME/.local/share/color/icc
    cd $HOME/.local/share/color/icc
    wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/CGATS21_CRPC1.icc
    wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/sRGB-elle-V2-srgbtrc.icc

If the Scribus files complain about a missing PHIL or PHILIPS color profile; skip it: this is the color profile of my monitor for the rendering of colors on my workstation. Replace it with your own monitor profile, or if your monitor is not calibrated; use a basic sRGB profile (eg: 'sRGB Built in' or 'none').

## Fonts

The repository [peppercarrot/fonts](https://framagit.org/peppercarrot/fonts) contains all the fonts needed to edit the book-publishing repository and the webcomic in any language. To install it quickly:

    cd
    mkdir $HOME/.fonts
    cd $HOME/.fonts
    git clone https://framagit.org/peppercarrot/fonts.git peppercarrot-fonts

To update it later if you add a new font or want to get the latest version:

    cd .fonts/peppercarrot-fonts
    git pull

## Workflow

### Editing

Once everything is installed and setup, edit the script ``./download.sh`` (this will be the main script of the project) with your text editor. This script is used to download all the source but also to render them all:

- it download the SVGs translation of each page

- it download the PNG artwork of each page (without speechbubble)

- it render a PNG lossless RGB page from the SVG and the PNG

- it converts this PNG to a CMYK tiff file with sharpening for print

Edit the line ``lang="en"`` to the lang of your choice on Pepper&Carrot if you don't want to download the English version. This way, the script will download only the SVG page in the target language. You'll notice also mask into the folder ``src/_mask`` this one define part of the pictures where language shouldn't be rendered. I use it to clean the attribution sometime included into the last panel on the first episodes. Title of the page will also be rendered this way.

If you want to edit the speechbubble; it is preferable to make the change on the translation project upstream. The download.sh script will catch later the change and render only what is necessary. If you do local edit, they might be lost if you run the script again. The script only comapre the version on the server and the local version; and in case of difference overwrite with the data of the server... Better to know that before making local changes.

The script ``./update-all`` is a tool I made for my disk, it allowed me to modify an artwork; launch that and get the webpage generated, the artworks uploaded, and download.sh launched to pick the result. I had to run that each time I wanted to add a new drawing for the Artbook. So, it was pretty necessary. 

If you run into issue of rendering; the ``cache`` folder might prevents re-rendering things. You can delete it to flush the cache. The cache is used to not make your computer process all the pages and all the files again. Only the one changed. 

You'll find on the `txt` folder the text that require translation. There is no way to sync them back into the Scribus text field automatically; you'll have to copy/paste them one by one. But working on the translation with the text file will allow you to share it with other translator without having to install all this setup.

### File naming

- all Scribus files starts with a two letter lang + _ (eg. 'en_').
- all Scribus files are identified by letter `B` + two letters (eg. B0A = artbook, B02 = Book 2).
- all Scribus files are or files of 'cover' or files of 'inside' (always different files in book printing).
- all this files are available into hardcover and softcover.
- for clarity; number of pages is written on 'inside' and 'cover' (revelant for thickness of cover too). 
- for clarity; the format in Inches is also written using the letter 'x' as a separator.

Notes: 'inside' files for softcover and hardcover are similar and produce similar PDFs.

### Exporting from Scribus

Once ready (the Scribus editing) you can export to PDF. Scribus will reset the path where you can export the file; you need to manually add "export" in the path and also Scribus sometime doesn't remember the last exported settings. Don't run multiple export in parallel or Scribus will crash. 
Important option that sometime are changed:

- Tab General: PDF/X-3, Compression method: Automatic, Quality: Maximum, Max Img res: 300dpi 

- Tab fonts: Click Embed all

- Tab prepress: check "Use document bleed", Infostring: but the same name as your filename.

### Sanify PDF for printer

Once the PDF generated; this one will not be accepted by the POD service because of multiple bugs in Scribus concerning PDF-X. 
You'll need to convert your exported PDF (especially the one 'inside' who bugs more) with Ghostscript.

If you exported your PDF into the 'export' folder; you'll find a script on it named ``./pdf-render.sh`` this script will take care of exporting everything:

- It will crop JPG for the cover on the shop (to create the product item) and put them back to RGB.

- It will create a Sample PDF of just the first pages, to propose it to read online as a sample.

- it will create the final RGB PDF you can sell as a digital download (glueing the cover+backcover and all the inside together in RGB, with a lightweight enough file size for tablet and e-reader).

- it will create \*_CMYK-sanified PDF files you can upload for the the "inside" that are usually rejected by the printer if coming directly from Scribus.


## License

[Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
Read LICENSE files hosted in the same folder, a detailed attribution for the art and proofreading is inside each book (Scribus file). If you can't open them, check https://www.peppercarrot.com/en/static6/sources&page=credits

Important: Authors of all modifications, corrections or contributions to this repository
accepts to release their work under the same license. This do not apply if you fork this repository.

## Derivations

If you publish a derivation of the book, please name your shop/publisher account after your own name (or own company/publisher name) and describe on the product it is a derivation from the official and that you are not the artist. It's important to communicate clearly with the audience. 

Also **please use your own logo on the cover of the books**; I do not accept people impersonating me, and this is the easiest way to avoid confusion...
