Title: Pepper&Carrot - Book 1: The Potion of Flight
Title: Pepper&Carrot - Book 2: The Witches of Chaosah
Title: Pepper&Carrot - Book 3: The Butterfly Effect
Author(s): David Revoy, the Pepper&Carrot community
Artist(s): David Revoy
Number of pages: 74

Selling price:
Hardcover: 16.50
Softcover: 12.30
PDF: 3.50
(later) Add-on Cost: 1.50

Product page text:
Pepper&Carrot is about "Pepper", a young witch and her cat, "Carrot". They live in a fun fantasy universe made of potions, magic and creatures. It's a comedy/humor webcomic suited for everyone. No mature content, no violence.This book contains episodes of the webcomic from 1 to 11 on 72 pages (plus a sketchbook bonus at the end), 8,5"x11" (21,59x27,94cm) on premium paper.

This book is self-published using only Free/Libre and Open-Source software and the webcomic is Free/Libre and Open-Source as well (licensed under the Creative Commons Attribution 4.0 International). Benefits of this book goes directly to the author, and are invested to maintain the project. Check the first pages on the preview to see the full credits for the Pepper&Carrot community.

Audience: General, Kid-Friendly
Genre: Fantasy, Sword & Sorcery, Humor
Product Type: Graphic Novels, Full Color, Web Comics, collections, Manga/Manwha
Format: PDF, Print, Comics
Languages: English
Source of PDF file: Electronic format
In-page Flipbook Preview
Flipbook preview enabled
	First page: <blank>
	Last page: 8
PDF preview enabled
	First page: 1
	Last page: 8
Stock Number: ENB0100

Purchase notes:
Thank you for purchasing "Pepper&Carrot - Book X: XXXXXXXXXXXXXX" and for your support! 
I hope you'll enjoy the book and you'll receive it soon.
David Revoy.
https://www.peppercarrot.com

Video URL: TODO

---

Upload:

(Add digital download PDF)
(Go back to menu > Add new format "Hardcover, Premium Color Book").

Print size: 8,5"x11"
(add hardcover plate PDF + Inside PDF CMYK)


