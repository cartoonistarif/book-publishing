#!/bin/bash
#: Title        : Bookpublishing all (tool for me)
# This tool is really specific to my workflow and install
# In short, it's convenient to update artworks on the book (if you have the source)
# 1. it browse all the place where I have sources/kra/svg files and render JPG/ZIP etc...
# 2. it upload all of that into the webserver of peppercarrot
# 3. it call back the bookpublishing script "download.sh" to transform the changes into CMYK
#    and store them on the book project.
#: Author       : David REVOY < info@davidrevoy.com >
#: License      : GPLv3 or higher

# colors
Off=$'\e[0m'
Red=$'\e[1;31m'
Green=$'\e[1;32m'
Blue=$'\e[1;34m'
Yellow=$'\e[1;33m'

# terminal windows title
printf "\033]0;%s\007\n" "Bookpublishing - Update all"

# Setup the root directory
input_path="${PWD}"

# Refresh repositories

# Artworks
echo "${Blue}################### UPDATE ARTWORKS ######################${Off}"
echo "${Blue}##########################################################${Off}"
cd "/home/deevad/peppercarrot/webcomics/0ther/artworks"
./kra-render.sh

# Sketchbook
echo "${Blue}################### UPDATE SKETCHBOOK ######################${Off}"
echo "${Blue}############################################################${Off}"
cd "/home/deevad/peppercarrot/webcomics/0ther/sketchbook"
./kra-render.sh

# Misc
echo "${Blue}################### UPDATE MISC ######################${Off}"
echo "${Blue}############################################################${Off}"
cd "/home/deevad/peppercarrot/webcomics/0ther/misc"
./kra-render.sh

# Vector
echo "${Blue}################### UPDATE VECTOR ######################${Off}"
echo "${Blue}########################################################${Off}"
cd "/home/deevad/peppercarrot/webcomics/0ther/vector/"
./vector-render.sh

# Book publishing
echo "${Blue}################### UPDATE BOOKPUBLISHING ######################${Off}"
echo "${Blue}################################################################${Off}"
cd "/home/deevad/peppercarrot/webcomics/0ther/book-publishing"
./kra-render_special-png.sh

echo "${Blue}################### UPLOAD ALL CHANGES TO WWW ##################${Off}"
echo "${Blue}################################################################${Off}"
cd "/home/deevad/peppercarrot/tools"
./uploader.sh

echo "${Blue}################### UPDATE BOOK ART CMYK/TIFF ##################${Off}"
echo "${Blue}################################################################${Off}"
cd "/home/deevad/peppercarrot/book-publishing"
./download.sh --nocomicrender --noprompt

# Print a end
echo "${Blue}================== JOBS DONE ==================${Off}"
echo "${Purple}  Press [Enter] to launch the script again or:"
echo "${White}${BlueBG}[q]${Off} => to Quit. ${Off}"
read -p "?" ANSWER
if [ "$ANSWER" = "q" ]; then
  exit
else
  # Launch again
  ${input_path}/update-all.sh
fi
